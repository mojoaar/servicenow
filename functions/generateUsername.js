function generateUsername(firstName,middleName,lastName) {
    /**
     * Generate a new username.
     * @param {string} firstName - The firstname of the user
     * @param {string} middleName - The middlename of the user
     * @param {string} lastName - The lastname of the user
     */
    var firstName = (firstName).substr(0, 1);
    var middleName = (middleName).substr(0, 1);
    var lastName = (lastName).substr(0, 1);
    var userName = firstName + middleName + lastName;
    gs.info("Generated username: " + username);
    var searchusers = new GlideRecord('sys_user');
    searchusers.addEncodedQuery('user_nameSTARTSWITH' + userName);
    searchusers.query();
    while (searchusers.next()) {
        // do something
        if (searchusers.getRowCount() > 0) {
            gs.info("Username exists");
        } 
    }
}